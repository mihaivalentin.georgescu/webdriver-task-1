describe("Test suite for https://pastebin.com/ website", () => {
    it("First test", async () => {
        await browser.navigateTo("https://pastebin.com/");
        await browser.pause(1000);

        const button_agree = await $("//button[@mode='primary']");
        const textarea = await $("textarea#postform-text");
        const page_expiration_never = await $("//span[text()='Never']");
        const page_expiration_10_minutes = await $("//li[text()='10 Minutes']");
        const post_form = await $("//input[@id='postform-name']");
        const create_paste_button = await $("//button[text()='Create New Paste']");

        button_agree.click();
        await browser.pause(1000);
        page_expiration_never.click();
        await browser.pause(1000);
        page_expiration_10_minutes.click();
        await browser.pause(1000);
        post_form.click();

        await browser.pause(1000);
        await textarea.setValue("Hello from WebDriver");
        
        create_paste_button.click();
        await browser.pause(5000);
    });
});